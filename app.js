import express from 'express';
import { createServer } from 'node:http';
import { fileURLToPath } from 'node:url';
import { dirname, join } from 'node:path';
import { Server } from 'socket.io';

const app = express();
const server = createServer(app);
const io = new Server(server);      // configuracion del socket como server

const __dirname = dirname(fileURLToPath(import.meta.url));

app.get('/', (req, res) => {
  res.sendFile(join(__dirname, 'index.html'));
});

io.on('connection', (socket) => {
    console.log('a user connected', socket.id);
});

io.on('connection', (socket) => {
    socket.on('chat message', (msg) => {
      console.log('message: ' + msg);
    });
});

// cada 2 segundos emite un valor aleatorio de morse (simulado)
// esto deberia ir despues de leer la camara e interpretar
// los parpados.
// en tu proyecto esta parte no va en un .js sino
// en el .py de la camara
setInterval(() => {
    io.emit('morse', morseSequence())  
}, 2000);

// funcion que simula un cod morse
const morseSequence = () => {
    var seq = '';
    
    for (var i = 0; i<4; i++) {
        seq += Math.random()>0.5 ? '.' : '-'
    }
    return seq + ' ';
};

server.listen(3000, () => {
  console.log('server running at http://localhost:3000');
});